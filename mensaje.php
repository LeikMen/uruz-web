<?php
$req = json_decode(file_get_contents('php://input'), true);

$mail = $req['mail'];
$cel = $req['cel'];
$mensaje = $req['msj'];

$destinatario="gmail"; 
$asunto = "mensaje de $mail";
$carta = "Email: $mail \nCelular: $cel \nMensaje: \n    $mensaje \n";

$header="From: no-reply@c2121063.ferozo.com\nReply-To:"."$mail";
$header .= "Mime-Version: 1.0\n";
$header .= "Content-Type: text/plain";

if(mail($destinatario, $asunto, $carta, $header)){
    $res = array('mensaje' => "Su mensaje fue enviado, muchas gracias.");
    header('Content-Type: application/json');
    echo json_encode($res);
}else{
    $res = array('mensaje' => "No se pudo mandar el mensaje, por favor intente de nuevo.");
    header('Content-Type: application/json');
    echo json_encode($res);
}
?>
