let navResp = document.getElementById("nav_responsive");
let navContL = document.getElementById("nav_cont_l");
let navItem = document.getElementsByClassName("nav_item");
let iNav = false;
const packageNumberInput = document.getElementById("packageNumber");
const sendButton = document.getElementById("sendButton");
const step1 = document.getElementById("step1");
const step2 = document.getElementById("step2");
const step3 = document.getElementById("step3");
const step4 = document.getElementById("step4");
const step4description = document.getElementById("step4description");
let sept4option;

// Desactivar menú contextual del botón derecho
document.oncontextmenu = function() { return false; }

// Configurar posición inicial del menú
navResp.style.top = "-40vh";
navResp.style.transition = "1s";

// Agregar eventos a los elementos de navegación
for (let item of navItem) {
    item.addEventListener('click', () => {
        navResp.style.top = "-40vh";
        iNav = false;
    });
}

// Agregar evento al contenedor de navegación
navContL.addEventListener('click', () => {
    if (!iNav) {
        navResp.style.top = "0";
        iNav = true;
    } else {
        navResp.style.top = "-40vh";
        iNav = false;
    }
})

// Ocultamos los pasos de la ruta inicialmente
step1.style.display = "none";
step2.style.display = "none";
step3.style.display = "none";
step4.style.display = "none";

// Agregamos un evento al botón "Enviar" para controlar la visibilidad de los pasos de la ruta
sendButton.addEventListener("click", function() {
    const packageNumber = packageNumberInput.value.trim();

    // Mostramos los pasos de la ruta según el número de paquete ingresado
    if (packageNumber === "123456") { // Aquí puedes poner el número de paquete que desees
        step1.style.display = "block";
        step2.style.display = "block";
        step3.style.display = "block";
        step4.style.display = "block";

        switch(sept4option){
            case "entregado":
                step4description.textContent = "Entregado";
                break;
            case "rechazado":
                step4description.textContent = "Paquete rechazado";
                break;
            case "ausente":
                step4description.textContent = "No había nadie en el domicilio";
                break;
            case "datos":
                step4description.textContent = "Faltan datos del domicilio";
                break;       
            default:
                step4description.textContent = "Pendiente";
        }

    } else {
        alert("Número de paquete incorrecto");
    }
});