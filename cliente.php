<?php
$req = json_decode(file_get_contents('php://input'), true);

$nom = $req['nom'];
$cel = $req['cel'];
$mail = $req['mail'];
$modX = $req['modX'];
$vecesX = $req['vecesX'];
$perX = $req['perX'];
$mesesX = $req['mesesX'];
$precio = $req['precio'];
$edad = $req['edad'];
$sexo = $req['sexo'];
$altura = $req['altura'];
$peso = $req['peso'];
$habitos = $req['habitos'];
$fumador = $req['fumador'];
$alcohol = $req['alcohol'];
$experiencias = $req['experiencias'];
$lesiones = $req['lesiones'];
$hist_lesiones = $req['hist_lesiones'];
$patologia = $req['patologia'];
$trab_est = $req['trab_est'];
$plazo_c = $req['plazo_c'];
$plazo_l = $req['plazo_l'];
$elementos = $req['elementos'];
$agregado = $req['agregado'];

$destinatario = $req['destinatario'];
$asunto = "planilla de $mail";
$mensaje = "Nombre: $nom \nCelular: $cel \nMail: $mail \n\nModalidad: $modX \nVeces: $vecesX \nCantidad de personas: $perX \nMeses a pagar:$mesesX \nTotal a pagar: $precio \n\nEdad: $edad años \nSexo: $sexo \nAltura: $altura mts. \nPeso: $peso kg. \nHábitos alimenticios: $habitos \nFuma: $fumador \nToma alcohol: $alcohol \nExperiencias con actividad fisica: $experiencias \nLesiones actuales: $lesiones \nHistorial de lesiones: $hist_lesiones \nCarga horaria de trabajo/estudio: $trab_est \nObjetivo a corto plazo: $plazo_c \nObjetivo a largo plazo: $plazo_l \nElementos para entrenar: $elementos \nAgregado adicional: $agregado /n";

$header="From: no-reply@c2121063.ferozo.com\nReply-To:"."$mail";
$header .= "Mime-Version: 1.0\n";
$header .= "Content-Type: text/plain";


if(mail($destinatario, $asunto, $mensaje, $header)){
    $res = array('mensaje' => "Su mensaje fue enviado, muchas gracias.");
    header('Content-Type: application/json');
    echo json_encode($res);
}else{
    $res = array('mensaje' => "No se pudo mandar el mensaje, por favor intente de nuevo.");
    header('Content-Type: application/json');
    echo json_encode($res);
}
?>
